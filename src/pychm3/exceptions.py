class BaseException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class MolError(BaseException):
    """
    The exception to raise when errors occur involving the :class:`Mol`
    class.
    """
    pass

class AtomError(BaseException):
    """
    The exception to raise when errors occur involving ``MetaAtom``, or
    derived classes.
    """

class StructError(BaseException):
  pass

class NoAlphaCarbonError(BaseException):
  pass

class ReaderError(BaseException):
  pass

class WriterError(BaseException):
  pass
