from pychm3.const import alphanum
from pychm3.tools import expandPath, cleanStrings, paragraphs
from pychm3.lib.atom import Atom
from pychm3.lib.model import Model

import os
import re

__all__ = [
  'PDB',
  'get_model_from_crd',
  'get_model_from_pdb'
]

def get_formatting(text, **kwargs):
    """
    Takes a string representing the path to a file containing molecular
    coordinate data, in either *.pdb* or *.crd* format, and attempts
    to return a string representing the exact formatting of that file.

    This is done by trying to apply an :class:`Atom`-like constructor
    to lines of text in a brute force manner.  If the constructor is
    called without raising an error the formatting is accepted.
    Possible return values are: *'pdborg', 'charmm', 'crd', 'xcrd',
    'unknown'*.

    **kwargs:**
        | ``atomobj`` Specify the :class:`Atom`-like constructor to use
        defaults to :class:`Atom`.
    """

    AtomObj = kwargs.get('atomObj', Atom)

    def crd_or_pdb():
        formatDict = {
            'pdborg': 'pdb',
            'charmm': 'pdb',
            'crd': 'crd',
            'xcrd': 'xcrd'
        }

        for k, v in list(formatDict.items()):
            for line in text.split("\n"):
                try:
                    AtomObj(line, format=k)
                    return v
                except:
                    pass
        return 'unknown'

    type = crd_or_pdb()

    if type == 'pdb':
        # differentiate between pdborg and charmm types

        lines = [line for line in text if line.startswith(('atom', 'hetatm'))]

        for line in lines:
            if line[21:22] == ' ' and line[72:73] in alphanum:
                return 'charmm'
            elif line[21:22] in alphanum and line[12:14].strip() == line[66:].strip():
                # this might have a funny corner case with a 'he22' type atomType
                return 'pdborg'

        return 'unknown'
    else:
        return type

def get_model_from_crd(crd_text, **kwargs):
    """
    A function that returns a single :class:`Mol` object from a single
    *.crd* text file.

    **kwargs:**
        | ``atomobj`` Specify the :class:`Atom`-like constructor to use
        defaults to :class:`Atom`.
        | ``informat`` Specify the plaintext formatting of the file,
        defaults to *'auto'*.
    """

    AtomObj = kwargs.get('atomObj', Atom)
    in_format = kwargs.get('format', 'auto')

    if in_format == 'auto':
        in_format = get_formatting(crd_text, **kwargs)

        if in_format == 'unknown':
            raise AssertionError('Unknown CRD format.')

    kwargs['format'] = in_format

    lines = [line.lower().rstrip() for line in crd_text]
    atom_objs = []

    for line in lines:
        try:
            atom = AtomObj(line, **kwargs)
            atom_objs.append(atom)
        except:
            pass

    return Model(atom_objs, **kwargs)

def get_model_from_pdb(pdb_text, **kwargs):
    """
    A function that returns a single :class:`Mol` object from a single
    *.pdb* text file.

    Useful if you do not need the additional features provided by a
    ``PDB`` object.  If the input .pdb text file contains more than one
    model, will return the first model.

    **kwargs:**
        *See* ``PDB`` *for valid kwargs.*
    """
    pdb = PDB(pdb_text, **kwargs)

    return pdb[0]

class PDB(object):
    """
    The ``PDB`` object serves as a container of ``Mol`` objects which are
    logically united and derive from the same plain text .pdb file.


    Calling the constructor on a plain text .pdb file instantizes one ``PDB``
    object and *n* ``Mol`` objects, one for each *model* present in the .pdb file.
    Furthermore, the meta-data contained in the .pdb file is parsed and stored in
    a dictionary for later use.  The ``Mol`` objects created at instantisation
    may be accessed by the ``PDB`` object in either a dictionary-like way or
    a list-like way.  Files which contain only one model will store the ``Mol``
    object as *model 0*.  Please note, that as of right now, the string keys for models
    are zero-padded.

    For example, to access *model 4* from the 1o1o.pdb file one could write:

    >>> taco = PDB('1o1o.pdb')[4]

    or...

    >>> taco = PDB('1o1o.pdb')['model04']

    Furthermore, this object may also be used to couple other ``Mol`` objects to
    each other.  Such as a structure that has been patched, or a minimized
    structure.  The syntax for doing this is dictionary-like, for example...

    >>> taco = PDB('1ski.pdb')
    >>> taco['lulz'] = PDB('1o1o.pdb')[4]

    Would couple the 4th model of the 1o1o structure to the ``taco`` object using
    the key ``'lulz'``.  Please note that keys begining with the string `'model'`
    are not allowed, nor are :class:`int` keys.  Using either of which will raise
    a :exc:`KeyError`.

    **kwargs:**
        | ``informat``      ['auto','pdborg','charmm']
        | ``fix_chainid``   [True,False]    # Attempts to fix mangled chainid
        | **TODO** ``fix_resid``     [True,False]    # Attempts to fix mangled resid
        | ``autofix``       [True,False]    # Flag for atom._autoFix
        | ``verbose``       [False,True]

    :TODO:
        | ``get_warnings`` :: Sophisticated warning handling
        | ``fix_resids`` :: Automagic resid mangling repairs
    """

    _autoInFormat = 'pdborg'
    """
    A string that defines the default input formatting for all class
    instances.
    """

    _reCode = re.compile(r"(?<![a-z0-9])[0-9][a-z0-9]{3}(?![a-z0-9])")
    """
    A regular expression for deriving the PDB code from the filename of
    a .pdb file named using CHARMMing conventions.
    """

    def __init__(self, pdb_text, **kwargs):
        in_format = kwargs.get('format', 'auto')
        fix_chainid = kwargs.get('fix_chainid', True)
        fix_resid = kwargs.get('fix_resid', True)

        self.__text = pdb_text
        self.__autoFix = kwargs.get('autoFix', True)
        self.__debug = kwargs.get('debug', False)

        self.warnings = []
        self._mols = {}

        # Partitioning
        self.__set_partitions()          # self.header / self.crd / self.footer

        # Input Formatting
        if in_format == 'auto':
            self.__in_format = self.__get_formatting()
        else:
            self.__in_format = in_format

        if self.__debug:
            print('%s: Input formatting set to `%s`' % (self.code, self.in_format))

        # auto fixing
        if fix_chainid:
            if self.__debug:
                print('%s: Fixing `chainid`s' % self.code)
            self.__fix_chainids()

        # TODO: Implement fix_resids
        if fix_resid:
            if self.__debug:
                print('%s: Fixing `resid`s' % self.code)
            # self._fix_resids()

        # Process PDB
        self.__build_models()

##############
# Properties #
##############

    @property
    def code(self):
        """
        A ``property`` for the PDB accession code of the ``PDB``.
        """
        lines = [line for line in self.header if line.startswith('header')]

        if lines:
            return lines[0].split()[-1]
        else:
            return '????'

    @property
    def crd(self):
        """
        A ``property`` for the atomic coordinate data from the .pdb
        file, in its unadulterated text form.
        """
        return self.__crd

    @property
    def filename(self):
        """
        A ``property`` for the name of the .pdb file from which this
        ``PDB`` instance was created.
        """
        if hasattr(self.__filename):
            return self.__filename

        return None

    @filename.setter
    def filename(self, filename):
        self.__filename = expandPath(filename)

    @property
    def header(self):
        """
        A ``property`` for the metadata in the .pdb file that precedes
        the atomic coordinate data.  This property gives access to its
        unadulterated text form.
        """
        return self.__header

    @property
    def footer(self):
        """
        A ``property`` for the metadata in the .pdb file that follows
        the atomic coordinate data.  This property gives access to its
        unadulterated text form.
        """
        return self.__footer

    @property
    def in_format(self):
        """
        A ``property`` for the formatting of the input .pdb text file
        data, either *"pdborg"* or *"charmm"*.
        """
        return self.__in_format

    @property
    def path(self):
        """
        A ``property`` for the dirname where the ``PDB`` instance's
        parent .pdb text file resides.
        """
        if self.filename:
            return os.path.dirname(self.filename)

        return None

    @property
    def models(self):
        if hasattr(self, '__models'):
            return self.__models

        model_keys = [key for key in sorted(self._mols.keys()) if key.startswith('model')]
        models = [self._mols[key] for key in model_keys]

        self.__models = models

        return models

##################
# Public Methods #
##################

    def get_warnings(self):
        """
        Return a list of warnings generated by the ``PDB`` object
        itself, and all of the ``Mol`` objects it contains.
        """
        result = self.warnings[:]
        for mol in self.iter_all():
            result.extend(mol.warnings)
        return result

    def get_metaData(self):
        """
        Returns a :class:`dict` containing metadata parsed from the
        ``PDB`` object's ``_header`` and ``_footer``.  Each key
        corresponds to a section in the header, delimited by the first
        string in each line.
        """
        tmp = {}
        for line in self.__header:
            key = line.split()[0]
            value = line.split(key)[1].lstrip()
            if key not in list(tmp.keys()):
                tmp[key] = [value]
            else:
                tmp[key].append(value)
        for line in self.__footer:
            key = line.split()[0]
            value = line.split(key)[1].lstrip()
            if key not in list(tmp.keys()):
                tmp[key] = [value]
            else:
                tmp[key].append(value)
        return tmp

    def iter_all(self):
        """
        Iterate over all of the ``Mol`` objects contained in the
        current ``PDB`` instance, regardless of origin.  *Models*
        are iterated upon before non-models.
        """
        return ( self[key] for key in list(self.keys()) )

    def iter_models(self):
        """
        Iterate over the ``Mol`` objects derived from the .pdb file's
        *models*.
        """
        models = [ key for key in sorted(self._mols.keys()) if key.startswith('model') ]
        return ( self[key] for key in models )

    def iter_notModels(self):
        """
        Iterate over the ``Mol`` objects **not** derived from the .pdb file's
        *models*.
        """
        notModels = [ key for key in sorted(self._mols.keys()) if not key.startswith('model') ]
        return ( self[key] for key in notModels )

    def keys(self):
        """
        Lists the keys to access all of the ``Mol`` objects the ``PDB``
        object contains.
        """
        models = [ key for key in sorted(self._mols.keys()) if key.startswith('model') ]
        notModels = [ key for key in sorted(self._mols.keys()) if not key.startswith('model') ]
        return models + notModels

###################
# Private Methods #
###################

    def __build_models(self):
        """
        Parse the crd section, and load the coordinates into :class:`Mol`
        objects, one :class:`Mol` object per model section in the .pdb file.
        """
        models = paragraphs(self.crd, splitter=['model'])

        for model in models:
            if model[0].startswith('model'):
                model_num = int(model[0].split()[1])
            else:
                model_num = 0

            atoms = (Atom(
                text=line,
                format=self.in_format,
                index=i,
                auto_fix=self.__autoFix
            ) for i, line in enumerate(model) if line.startswith(('atom', 'hetatm')))

            self._mols['model{:02d}'.format(model_num)] = Model(
                iterable=atoms,
                name='model{:02d}'.format(model_num),
                code=self.code,
                auto_fix=True
            )

    def __fix_chainids(self):
        """
        Detect and correct chainid mangling.
        """
        # Detect mangling
        def gen():
            for line in self.crd:
                if line.startswith(('atom', 'hetatm')):
                    atom = Atom(text=line, format=self.in_format, autofix=True)
                else:
                    continue
                yield atom.chain_id

        # Correct mangling
        if '' in set(gen()):
            # Throw a warning if chainids are mangled
            self.warnings.append('chainids mangled')
            print('One or more chainids appear to be missing, attempting to\
                    autofix.\n')
            print('Please verify the accuracy of your .pdb file upon\
                    completion.\n')

            chain_num = 0

            for i, line in enumerate(self.crd):
                if line.startswith(('atom', 'hetatm')):
                    tmp = Atom(text=line, format=self.in_format, autofix=True)
                    # Blank chainid
                    if not tmp.chain_id:
                        tmp.chain_id = alphanum[chain_num]
                        self.crd[i] = tmp.to_str(format=self.in_format).lower()
                elif line.startswith('ter'):
                    chain_num += 1
                elif line.startswith('model'):
                    chain_num = 0
                else:
                    continue

    def __fix_resids(self):
        """
        Detect and correct resid mangling.
        """
        raise NotImplementedError

    def __get_formatting(self):
        """
        Wrapper method to detect PDB text formatting, defaults to 'pdborg'
        """
        result = get_formatting(self.__text)

        if result == 'unknown':
            # Throw a warning if formatting is guessed.
            self.warnings.append('Undetected pdb formatting')
            return self.__class__._autoInFormat
        else:
            return result

    def __set_partitions(self):
        """
        Partition the file into _header/_crd/_footer sections.
        """
        lines = (line for line in cleanStrings(self.__text.split("\n")))

        # Populate header
        self.__header = []
        tmp = None

        for line in lines:
            if line.startswith(('atom', 'hetatm', 'model')):
                tmp = line
                break
            else:
                self.__header.append(line)

        # Populate coordinates
        if tmp is None:
            self.__crd = []
        else:
            self.__crd = [tmp]

        tmp = None
        for line in lines:
            if not line.startswith(('atom', 'anisou', 'hetatm', 'model', 'ter', 'endmdl')):
                tmp = line
                break
            else:
                self.__crd.append(line)

        # Populate footer
        if tmp is None:
            self.__footer = []
        else:
            self.__footer = [tmp]

        for line in lines:
            self.__footer.append(line)

###################
# Special Methods #
###################

    def __getitem__(self, key):
        if type(key) == int:
            try:
                return self._mols['model{:02d}'.format(key)]
            except KeyError:
                raise KeyError('model{:02d}'.format(key))
        else:
            return self._mols[key]

    def __setitem__(self, key, value):
        if type(key) == int:
            raise KeyError('Integer keys are protected.')
        if key.startswith('model'):
            raise KeyError('Keys begining with "model" are protected.')
        self._mols[key] = value

    def __len__(self):
        return len(self._mols)

    def __contains__(self, key):
        return key in list(self.keys())

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, list(self.keys()))
