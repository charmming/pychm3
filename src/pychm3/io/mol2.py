from pychm3.tools import Property, cleanStrings
from pychm3.lib.bond import Bond
from pychm3.lib.model import Model
from pychm3.lib.atom import Atom

class MOL2Bond(Bond):
    def writeOut(self):
        return '%6s%6s%6s%5s' % (self.seq, self.i.number, self.j.number, self.order)

class MOL2(object):
    def __init__(self, mol_text, **kwargs):
        self.__text = mol_text
        self._partition()
        self._buildmodel()

    def _partition(self):
        inHeader = False
        inCrd = False
        inBonds = False

        iterator = ( line for line in cleanStrings(self.__text.split("\n")) )

        self.__header = []
        self.__crd = []
        self.__bonds_text = []

        for line in iterator:
            if not line:
                continue

            line = line.lower()

            if line.startswith('@<tripos>'):
                sectionName = line[9:].strip()
                if sectionName == 'molecule':
                    inHeader = True
                    inCrd = False
                    inBonds = False

                    if self.__header:
                        raise('Multiple header sections')
                elif sectionName == 'atom':
                    inHeader = False
                    inCrd = True
                    inBonds = False
                elif sectionName == 'bond':
                    if not self.__crd:
                        raise('')

                    inHeader = False
                    inCrd = False
                    inBonds = True
                else:
                    # Throw away other sections
                    inHeader = False
                    inCrd = False
                    inBonds = False
            else:
                if inHeader:
                    self.__header.append(line)
                elif inCrd:
                    self.__crd.append(line)
                elif inBonds:
                    self.__bonds_text.append(line)

    def _buildbonds(self):
        self.__bonds = []

        for bondline in self.__bonds_text:
            seq, atmi, atmj, order = bondline.split()
            tmp_bond = MOL2Bond(self.__model[int(atmi)-1], self.__model[int(atmj)-1])
            tmp_bond.seq = seq
            tmp_bond.order = order
            self.__bonds.append(tmp_bond)

    def _buildmodel(self):
        iterator = ( Atom(text=line, format='mol2', index=i) for i, line in enumerate(self.__crd) )
        self.__model = Model(iterable=iterator, name='model00', autoFix=True)
        self._buildbonds()

    def write_out(self, filename, **kwargs):
        pass

    @Property
    def model():
        def fget(self):
            return self.__model
        return locals()

    @Property
    def header():
        def fget(self):
            return self.__header
        return locals()

    @Property
    def bonds():
        def fget(self):
            return self.__bonds
        return locals()
