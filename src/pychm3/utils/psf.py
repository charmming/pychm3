from pychm3.lib.toppar import TopRes

import datetime
import re

def writePSF(mol, top, **kwargs):
    """
    Takes a list of :class:`RTFFile` objects (top).
    Automatically writes per-segment files as opposed to a full PSF for the structure
    unless user specifies to write a single seg. Segments are automatically written by their segids
    unless specified otherwise in the "segids" kwarg. CHEQ and CMAP are set by default to True,
    these vars depend on user's specific purpose for the PSF file. EXT determines whether this
    PSF is CHARMM extended format, by default set to True, otherwise raising a :class:`NotImplementedError` exception.
    The kwarg `out_file` will set the output PSF's name, otherwise it will be set to "psf_out.psf" by default
    for the full :class:`Mol` object's PSF, or "%segid%psf_out.psf" for each segment.
    The kwarg `username` sets the user in the title to that variable, else the PSF will read
    "CREATED BY USER: PSF_WRITER".
    PATCH contains the patches per segment, if any. format - {%segid%:{'first':%firsrtpatch,'last':%lastpatch%}

    Recommended forcefield: CGenFF
    Author: VS
    **kwargs**:
        | ``write_seg``  [False,True]
        | ``CHEQ``       [False,True]
        | ``CMAP``       [False,True]
        | ``EXT``        [False,True]
        | ``PSF_FORMAT`` ["old","new","short"]
        | ``PATCH``
        | ``out_file``
        | ``username``
        | ``segids``

    """
    #case insensitive kwargs
    write_seg = kwargs.get('write_seg',False) #write per-segment PSFs instead of a large one
    CHEQ = kwargs.get('cheq',True)
    CMAP = kwargs.get('cmap',True)
    EXT = kwargs.get('ext',True)
    out_file = kwargs.get('out_file',"psf_out.psf")
    username = kwargs.get('username',"PSF_WRITER")
    segids = kwargs.get('segids',{}) #expects full dict as input
    PATCH = kwargs.get('patch',{})
    PSF_FORMAT = kwargs.get('psf_format',"old")
    if PSF_FORMAT not in ["old","new","short"]: #if you put in something invalid, just use default values
        PSF_FORMAT = "old"
    if len(out_file.split(".")) > 1:
        out_base = out_file.split(".")[-1]
    else:
        out_base = out_file
    if not EXT:
        raise NotImplementedError("The pychm PSF writer does not currently support short PSF format.")
    #since file writes are slow, we stick everything into a big string and write it all at once
    currRTF = top[0]
    for i in range(1,len(top)):
        currRTF = currRTF.append(top[i]) #then we have all the RTFs appended to each other
    curr_date = datetime.datetime.now()
    date_day = "%s/%s/%s"%(curr_date.month,curr_date.day,curr_date.year) #change this for international use??
    date_sec = "%s:%s:%s"%(curr_date.hour,curr_date.minute,curr_date.second)
    atom_lines = ""
    lonepair_lines = "%10s%10s !NUMLP NUMLPH\n\n"%(0,0) #these should almost alwys be 0, they get set otherwise due to QM/MM type stuff, lonepairs
    # crterm_lines = "%10s !NCRTERM: cross-terms\n\n"%(0) #cross-terms...unsure when these show up
    title_lines = "%10s !NTITLE\n* psftitlehere\n*%7s%12s%13s%22s %s\n\n"%("2","DATE:",date_day,date_sec,"CREATED BY USER:",username)
    psf_string = "PSF %s %s %s %s\n\n"%("EXT" if EXT else "","CMAP" if CMAP else "","CHEQ" if CHEQ else "","XPLOR" if PSF_FORMAT == "new" else "")
    atomcount = 1
    bonds_dict = {} #this will have one key per atom in the PSF...
    #include PSF_FORMAT in lines_dict for argument modularity
    #we don't want to rewrite all our functions over and over again
    lines_dict = {'PSF_FORMAT':PSF_FORMAT,'bonds':[],'angles':[],'dihe':[],'impr':{},'donors':{},'acce':{},'groups':[],'atoms':atom_lines,'cmap':{},'title':title_lines,'lonepair':lonepair_lines}
    # print(list(iter_seg()))
    seg_list = sorted(mol.segments, key=lambda x: x[0].atomNum)
    # seg_list = sorted(list(iter_seg()),key=lambda x: next(next(x.iter_res()).iter_atom()).atomNum) #we need to sort the list or the order won't match generated CRDs...
    for seg in seg_list:
        #we need this to find the first and the last one, for patching
        for res in segment.residues:
            res.make_compliant()
        res_count = 1
        # bond_lines="%10s !NBOND: bonds\n"%"bondnum" #replace this later with actual bond number
        #we absolutely need PATCH information, even if it ends up being NONE
        use_rtf = False #this is the rtf we actually are going to use for this segment. We should NOT need more than one per segment, but we may redo this for bad segs
        if seg.segid not in PATCH:
            PATCH[seg.segid] = {}
        if not('first' in PATCH[seg.segid] or 'last' in PATCH[seg.segid]): #fill in PATCH with missing patches for this seg, if any
            for rtf in top: #this check is needed because of patching.
                #first check if this seg uses this RTF
                if res_list[0].resName in rtf.resi:
                    #ok, it does have it - check for default patches
                    use_rtf = True #keep track of the one that we need...
                    if rtf.defa:
                        patches = rtf.defa[0].split() #ok, now check for custom patches...
                        if "first" not in PATCH[seg.segid]:
                            PATCH[seg.segid]['first'] = patches[2]
                        if "last" not in PATCH[seg.segid]:
                            PATCH[seg.segid]['last'] = patches[4]
                    else: #no default patches? set to NONE if no custom patches were supplied
                        if "first" not in PATCH[seg.segid]:
                            PATCH[seg.segid]['first'] = "none"
                        if "last" not in PATCH[seg.segid]:
                            PATCH[seg.segid]['last'] = "none"
        if not use_rtf:
            raise IOError("Could not find residue %s in any of your provided RTF files."%res_list[0].resName)
        use_rtf = currRTF
        patchfirst = PATCH[seg.segid]['first']
        patchlast = PATCH[seg.segid]['last']
        if write_seg:
            atomcount = 1 #only refresh the count per-segment IF we are writing per-segment
            atom_lines = ""
            bonds_dict = {} #reset bonds...maybe reset lines dict too.
            #include PSF_FORMAT in lines_dict for argument modularity
            #we don't want to rewrite all our functions over and over again
            lines_dict = {'PSF_FORMAT':PSF_FORMAT,'bonds':[],'angles':[],'dihe':[],'impr':{},'donors':{},'acce':{},'groups':[],'atoms':atom_lines,'cmap':{},'title':title_lines,'lonepair':lonepair_lines}
        #need lists of the following to create full bond lists &c.
        #since these are per-segment, we need to move them around a bunch
        fpatch_rtf = use_rtf.pres[patchfirst] if patchfirst != 'none' else False
        lpatch_rtf = use_rtf.pres[patchlast] if patchlast != 'none' else False
        for i in range(0,len(res_list)):
            # print("On residue %s of segment %s"%(i,seg.segid))
            res = res_list[i]
            res.resid = res_count #number from 1
            res_count += 1
            internal_atomcount = atomcount
            amount_to_add = 0 #so we don't lose track of our real atomcount
            res_atoms = {}
            # res_atoms = dict(( (k.atomType.strip(), k.atomNum) for k in res.iter_atom() ))
            # print(res_atoms)
            if res.resName not in use_rtf.resi:
                raise NotImplementedError("Your residue could not be found in the supplied RTF. Multi-RTF systems not supported yet.")
            res_rtf = use_rtf.resi[res.resName]
            rtf_dict = {'bigRTF':use_rtf,'thisResRTF':res_rtf}
            patchinfo = {"active_rtfs":[],"delrecords":[]}
            if "patching" in res_rtf.meta or "patc" in res_rtf.meta: #unsure if it's always PATCHING
                respatchinfo = res_rtf.meta['patching'] if "patching" in res_rtf.meta else res_rtf.meta['patc']
                for prtf in respatchinfo: #might be more than one
                    check_prtf = prtf.split() #not sure how RTFFile class handles these
                    if ("firs" in check_prtf or "first" in check_prtf) and i==0:
                        fpatch_rtf = use_rtf.pres[check_prtf[1]] if check_prtf[1] != "none" else False
                    if "last" in check_prtf and i==len(res_list)-1:
                        lpatch_rtf = use_rtf.pres[check_prtf[1]] if check_prtf[1] != "none" else False #these should be defined if they come from the same rtf...
            rtf_dict['fpatch']= fpatch_rtf if i==0 else False
            rtf_dict['lpatch']= lpatch_rtf if i==len(res_list)-1 else False
            if rtf_dict['fpatch']:
                for del_rec in rtf_dict['fpatch'].meta['delete']:
                    patchinfo["delrecords"].append(del_rec.split()[1:])
                patchinfo['active_rtfs'].append("fpatch")
            if rtf_dict['lpatch']:
                for del_rec in rtf_dict['lpatch'].meta['delete']:
                    patchinfo["delrecords"].append(del_rec.split()[1:])
                patchinfo["active_rtfs"].append("lpatch")
            rtf_dict['patchinfo'] = patchinfo
            # print(amount_to_add)
            res_dict = {'res':res,'res_atoms':res_atoms,'segid':seg.segid}
            if i == 0: #first do an fpatch write check
                if rtf_dict['fpatch']: #remember FIRST NONE LAST NONE
                    for group in rtf_dict['fpatch'].groups:
                        # print(group)
                        for atom in group:
                            res_atoms[atom[0]] = atomcount
                            # print(atom[0])
                            lines_dict = fix_atom_params(lines_dict,rtf_dict,res_dict,atom[0],atomcount)
                            atomcount += 1
                            amount_to_add += 1
            for group in res_rtf.groups: #this is really inefficient but it's the price you have to pay for the fact that you need to look "ahead" at the other two residues before you can write atoms
                for atom in group:
                    bad_atom = False
                    for delrec in patchinfo["delrecords"]:
                        for delatom in delrec:
                            if delatom == atom[0]: #whoops, shouldn;'t be here
                                bad_atom = True
                    if not bad_atom and atom[0] not in res_atoms:
                        res_atoms[atom[0]] = atomcount #replicate original behavior...
                        lines_dict = fix_atom_params(lines_dict,rtf_dict,res_dict,atom[0],atomcount) #don't send out res_dict until it's been filled with the atom it needs...
                        atomcount += 1
                        amount_to_add += 1
            if i == len(res_list)-1:
                if rtf_dict['lpatch']:
                    for group in rtf_dict['lpatch'].groups:
                        for atom in group:
                            if atom[0] not in res_atoms:
                                res_atoms[atom[0]] = atomcount
                                lines_dict = fix_atom_params(lines_dict,rtf_dict,res_dict,atom[0],atomcount)
                                atomcount += 1
                                amount_to_add += 1
            # elif i == len(res_list) -1:
            #     if lpatch_rtf:
            #         for group in lpatch_rtf.groups:
            #             amount_to_add += len(group)
            # print(amount_to_add)
            # rtf_to_send = fpatch_rtf if i==0 else lpatch_rtf if i==len(res_list)-1 else res_rtf
            #need the atoms to be in order, but also need the dict so I can do correlations for bonds...
            #now get the previous and next residues so we can do cross-residue bonding
            if i != len(res_list) -1:
                nextres = res_list[i+1]
                if nextres.resName not in use_rtf.resi:
                    raise IOError("Residue %s:%s in segment %s could not be found in any RTF file provided."%(nextres.resid,nextres.resName,seg.segid))
                else:
                    res_dict['next'] = {}
                    check_res = use_rtf.resi[nextres.resName]
                    for group in check_res.groups:
                        for atom in group: #don't do atom processing, just focus on getting the numbering down
                            #check atom against delrecords
                            bad_atom = False
                            for delrec in patchinfo["delrecords"]:
                                for delatom in delrec:
                                    if delatom == atom[0]: #whoops, shouldn;'t be here
                                        bad_atom = True
                            if not bad_atom:
                                res_dict['next'][atom[0]] = internal_atomcount + amount_to_add
                                amount_to_add += 1 #then just keep it going up...
                    #ok, now check if it's lpatch, and apply lpatch if it is
                    if i+1 == len(res_list)-1 and lpatch_rtf: #make sure fpatch_rtf and lpatch_rtf are NOT none...because they might be!
                        for group in lpatch_rtf.groups:
                            #no atoms here are going to be in delrecords...
                            for atom in group:
                                res_dict['next'][atom[0]] = internal_atomcount + amount_to_add
                                amount_to_add += 1
            else:
                res_dict['next'] = False
            internal_atomcount -= 1
            if i != 0:
                prevres = res_list[i-1]
                if prevres.resName not in use_rtf.resi:
                    raise IOError("Residue %s:%s in segment %s could not be found in any RTF file provided."%(prevres.resid,prevres.resName,seg.segid))
                else:
                    res_dict['prev'] = {}
                    check_res = use_rtf.resi[prevres.resName]
                    # if i-1 == 0: #i-1 cannot be the last residue, because we are checking "behind"
                    #     check_res = fpatch_rtf
                    # print(check_res.groups)
                    for group in reversed(check_res.groups): #invert the order of the group list - even if patched!
                        for atom in reversed(group): #invert the order of all the individual lists
                            #check atom against delrecords
                            bad_atom = False
                            # for delrec in patchinfo["delrecords"]:
                            #     for delatom in delrec:
                            #         if delatom == atom[0]: #whoops, shouldn;'t be here
                            #             print(atom[0])
                            #             bad_atom = True
                            # NOTE: Not sure if this is actually needed...?
                            if not bad_atom:
                                # print(internal_atomcount)
                                # print(atom[0])
                                res_dict['prev'][atom[0]] = internal_atomcount
                                internal_atomcount -= 1 #then just keep it going up...
                    #ok, now check if it's fpatch, and apply fpatch if it is
                    if i-1 == 0 and fpatch_rtf: #it's possible that there just isn't one
                        for group in fpatch_rtf.groups:
                            #no atoms here are going to be in delrecords...
                            for atom in group:
                                res_dict['prev'][atom[0]] = internal_atomcount + amount_to_add
                                amount_to_add += 1
                            res_dict['prev'][atom[0]] = internal_atomcount
                            internal_atomcount -= 1
            else:
                res_dict['prev'] = False
            # this should hopefully deal with all patching problems.
            # print(lines_dict['atoms'])
            lines_dict = fix_other_params(lines_dict,rtf_dict,res_dict)
        # now that we're done with all the residues, NOW we can do the angle/dihe generation, which is NOT a parsing job
       # if write_seg: #execute FOR EACH segment
           # lines_dict = autogen(lines_dict,atomcount) #we don't need RTF, this is a purely combinatorial thing
        if write_seg:
            write_output(lines_dict,out_base+"-"+seg.segid+".psf",atomcount,psf_string)
    if not write_seg:
        write_output(lines_dict,out_file,atomcount,psf_string)

def write_lines(lines,data,per_unit,per_line):
    w_index = 1
    for datum in data:
        for i in range(0,per_unit):
            lines += "%10s"
        lines= lines%datum
        if w_index == per_line:
            lines += "\n"
            w_index = 1
            continue
        w_index += 1
    lines += "\n" if w_index == 1 else "\n\n"
    return lines

def write_output(lines_dict,out_file,atomcount,psf_string):
    """
    Auxiliary function for writing the actual PSF.
    Takes the dictionary with all the different "lines" elements, the file to write out to,
    the current count of atoms, and the introductory remarks in the PSF already established,
    then writes a PSF at out_file.
    """
    lines_dict = autogen(lines_dict,atomcount) #execute AFTER all segments
    bondlength = len(lines_dict['bonds'])
    bond_lines = "\n%10s !NBOND: bonds%s"%(bondlength,"\n" if bondlength > 0 else "\n\n")
   # bond_lines = write_lines(bond_lines,lines_dict['bonds'],2,4) #2 elem per bond, 4 bond per line
    b_index = 1 #can only write 4 bonds per line
    for bond in lines_dict['bonds']:
        bond_lines += "%10s%10s"%(bond.pop(),bond.pop())
        if b_index == 4:
            bond_lines += "\n"
            b_index = 1
            continue
        b_index += 1
    bond_lines += "\n" if b_index == 1 else "\n\n" #b_index will not be 4, it will be 0 after the \n gets written to the line with 4 bonds...

    anglength = len(lines_dict['angles'])
    angle_lines = "%10s !NTHETA: angles%s"%(anglength,"\n" if anglength > 0 else "\n\n")
    a_index = 1
    for angle in lines_dict['angles']:
        angle_lines += "%10s%10s%10s"%tuple(angle)
        if a_index == 3:
            angle_lines += "\n"
            a_index = 1
            continue
        a_index += 1
    angle_lines += "\n" if a_index == 1 else "\n\n" #ditto

    dihelength = len(lines_dict['dihe'])
    dihe_lines = "%10s !NPHI: dihedrals%s"%(dihelength,"\n" if dihelength > 0 else "\n\n")
    d_index = 1
    for dihe in lines_dict['dihe']:
        dihe_lines += "%10s%10s%10s%10s"%dihe
        if d_index == 2:
            dihe_lines += "\n" #2 dihe per line
            d_index = 1
            continue
        d_index += 1
    dihe_lines += "\n" if d_index == 1 else "\n\n"

    imprlength = len(lines_dict['impr'])
    impr_lines = "%10s !NIMPHI: impropers%s"%(imprlength,"\n" if imprlength > 0 else "\n\n")
    impr_lines = write_lines(impr_lines,iter(lines_dict['impr'].values()),4,2) #4 elem per impr, 2 per line

    cmaplength = len(lines_dict['cmap'])
    cmap_lines = "%10s !NCRTERM: cross-terms%s"%(cmaplength,"\n" if cmaplength > 0 else "\n\n")
    cmap_lines = write_lines(cmap_lines,iter(lines_dict['cmap'].values()),8,1) #8 elem per cmap, 1 per line

    donlength = len(lines_dict['donors'])
    dono_lines = "%10s !NDON: donors%s"%(donlength,"\n" if donlength > 0 else "\n\n")
    dono_lines = write_lines(dono_lines,iter(lines_dict['donors'].values()),2,4) #2 elem per dono, 4 per line

    accelength = len(lines_dict['acce'])
    acce_lines = "%10s !NACC: acceptors%s"%(accelength, "\n" if accelength > 0 else "\n\n")
    acce_lines = write_lines(acce_lines,iter(lines_dict['acce'].values()),2,4) #2 elem per acce, 4 per line

    grplength = len(lines_dict['groups'])
    grp_lines = "%10s%10s !NGRP NST2%s"%(grplength,0,"\n" if grplength > 0 else "\n\n")
    grp_index = 1
    for group in lines_dict['groups']:
        grp_lines += "%10s%10s%10s"%(group[0],group[1],group[2])
        if grp_index == 3:
            grp_lines += "\n"
            grp_index = 1
            continue
        grp_index += 1
    grp_lines += "\n" if grp_index == 1 else "\n\n"
    nnb_lines = "%10s !NNB\n\n"%(0) #figure these out later
    i = 1
    while i < atomcount:
        nnb_lines += "%10s"
        if i % 8 == 0:
            nnb_lines += "\n"
        i += 1
    add_lines = "\n" if (i-1) % 8 == 0 else "\n\n"
    nnb_lines += add_lines
    molnt_lines = "%10s !MOLNT\n"%(1) #no clue what makes this 1 vs 2 vs ???, 1 seems to be deafult for proteins, setting to 1
    i=1
    while i < atomcount:
        molnt_lines += "%10s"%(1)
        if i % 8 == 0:
            molnt_lines += "\n"
        i += 1
    add_lines = "\n" if (i-1) % 8 == 0 else "\n\n"
    molnt_lines += add_lines
   # print(out_file)
    lines_dict['atoms'] = "%10s !NATOM\n"%(atomcount-1) + lines_dict['atoms']
    psf_string = psf_string + lines_dict['title'].replace("psftitlehere",out_file) + lines_dict['atoms'] + bond_lines + angle_lines\
            + dihe_lines + impr_lines + dono_lines + acce_lines + nnb_lines + grp_lines + molnt_lines + lines_dict['lonepair'] + cmap_lines
    psf_file = open(out_file,"w")
    psf_file.write(psf_string)
    psf_file.close()
    #whether we write one or a bunch, we just stitch the lines together and print, the only part that changes is the title_lines, everything else
    #because it's separated into variables, stays exactly the same
   # psf_string += "%10s !NTITLE\n"%(len(title_lines))

def autogen(lines_dict,atomcount):
    ang_list = {} #this one keeps track of sets so you don't repeat yourself with same values - we use a dictionary with frozensets as keys
    real_ang_list = [] #this one keeps tuples with the actual order, such that CHARMM doesn't have erroneous values
    dihe_list = {} #keeps track of elements
    real_dihe_list = [] #keeps track of actual order...make sure we don't do 1 2 3 4 and 4 3 2 1 for example.
    bonds_dict = {key : [] for key in range(1,atomcount)} #atomcount is already too big, so we're good
    for i in range(0,len(lines_dict['bonds'])):
        curr_bond = lines_dict['bonds'][i]
        for elem in curr_bond: #get each element
            bonds_dict[elem].append(curr_bond) #add it to the dict part
    #so now we have a bond dictionary with each of the bonds...let's generate some angles.
    for i in range(1,atomcount):
        curr_bondset = bonds_dict[i] #get all bonds attached to the current atomno
        j=0
        for j in range(0,len(curr_bondset)-1):
            left_bond = curr_bondset[j]
            for k in range(j+1,len(curr_bondset)):
                right_bond = curr_bondset[k]
                union_bond = left_bond | right_bond
                if len(union_bond) == 3:
                    frozen_bond = frozenset(union_bond)
                    if frozen_bond not in ang_list:
                        ang_list[frozen_bond] = True
                        intersect = left_bond & right_bond
                        real_ang_list.append(((left_bond - right_bond).pop(),intersect.pop(),(right_bond - left_bond).pop()))
                    ob1 = (right_bond - left_bond).pop()
                    ob2 = (left_bond - right_bond).pop()
                    #now do one loop in ob1 and one loop in ob2 to check for stuff to make dihedrals out of
                    #ob1 and ob2 are "stretching the bond out in one direction"
                   # print(union_bond)
                   # print("ob1: %s =ob1 bonds= %s"%(ob1,bonds_dict[ob1]))
                   # print("ob2: %s =ob2 bonds= %s"%(ob2,bonds_dict[ob2]))
                    for bond3 in bonds_dict[ob1]:
                        dihe_list, real_dihe_list = get_dihe(union_bond,left_bond,right_bond,bond3,dihe_list,real_dihe_list)
                       # print(dihe_list)
                    for bond3 in bonds_dict[ob2]:
                        dihe_list, real_dihe_list = get_dihe(union_bond,left_bond,right_bond,bond3,dihe_list,real_dihe_list)
    lines_dict['angles'] = real_ang_list #add them all to here.
    lines_dict['dihe'] = real_dihe_list
    return lines_dict

def get_dihe(union_bond,left_bond,right_bond,bond3,dihe_list,real_dihe_list):
    """
    Auxiliary function for autogen.
    Since we need to go "one bond out" in two directions, it's best to pass this off to another function
    instead of writing this code twice.
    """
    union_ang = union_bond | bond3
   # print("left bond: %s"%left_bond)
   # print("right bond: %s"%right_bond)
   # print("third bond: %s"%bond3)
   # print("dihe union: %s"%union_ang)
    #now we test that this isn't three bonds on a single atom
   # t1 = left_bond & right_bond
   # t2 = left_bond & bond3
   # print("t1: %s t2: %s"%(len(t1),len(t2)))
    #at least one of these needs to be len(0) to have a proper dihedral, otherwise, we have problems
    #the t1 check may be taken out because of the other checks performed above, though
    if len(union_ang) == 4:
        frozen_dihe = frozenset(union_ang) #this should be fine...
        #dihedrals are unique by composition - are sets really the best option? unsure.
        #check that it's not in the dihe list
        if frozen_dihe not in dihe_list:
            other_union = bond3 | right_bond
           # outliers = (union_ang)-(left_bond&right_bond)-(right_bond&bond3)-(bond3&left_bond) #keep this here - slower, but more accurate route.
            outliers = left_bond^right_bond^bond3 #symmetric difference
           # if len(outliers) == 2: #this is such a small check
           # outlier1 = other_union - union_bond
           # outlier2 = union_bond - other_union
            #the centrals are gone from these two, now we need to identify which bond they're attached to
            o1 = outliers.pop()
            o2 = outliers.pop()
            outlier1 = set([o1])
            outlier2 = set([o2])
           # for x in outlier1:
           #     o1 = x #single element, so no worries
           # for y in outlier2:
           #     o2 = y #make sure we don't pop so we can still do set operations
           # #check the three bonds to see which one we take...central bond will be ignored
           # print("o1: %s o2: %s"%(o1,o2))
            b1 = False if o1 == 0 else (o1,(left_bond-outlier1).pop()) if len(outlier1 & left_bond) > 0 else (o1,(right_bond-outlier1).pop()) if len(outlier1 & right_bond) > 0\
                    else (o1,(bond3-outlier1).pop()) #if it's not in any of the three, there's a problem, but we should skip the last check in the interest of speed
            b2 = False if o2 == 0 else ((left_bond-outlier2).pop(),o2) if len(outlier2 & left_bond) > 0 else ((right_bond-outlier2).pop(),o2) if len(outlier2 & right_bond) > 0\
                    else ((bond3-outlier2).pop(),o2)
            #identify which bond is the one with the centrals...
            if b1 and b2: #still no clue why they sometimes mess up...
                dihe_list[frozen_dihe] = False
                real_dihe_list.append(b1+b2)
        # a dihedral is essentially the intersection of two angles, with two 'central' elements
        #and two 'outlier' elements. we take the two central elements and put them into their own list, and sort them,
        #then take the two outlier elements, and sort those
        #and then make a four-element tuple that has them both in the same order
        #for example:
        #1 3 2 4 is NOT the same as 1 2 3 4
        #but
        #1 2 3 4 is the same as 4 3 2 1
        #if the outlier elements are kept in the same order as the central elements, we have a dihedral that is sensible
    return dihe_list,real_dihe_list

def fix_other_params(lines_dict,rtf_dict,res_dict):
    """
    Auxiliary function for writePSF
    Takes a :class:`dict` object, one :class:`RTFFile` object, a :class:`res` object and lists to carry
    the different groups of lines that are needed for the PSF, and fills these lists with data obtained
    from the RTF file, filling in the BOND, THETA, PHI, IMPHI, DON, ACC lines of the PSF.
    Angles are autogenerated.
    """
    patchinfo = rtf_dict['patchinfo']
    atom_dict = {'prev':res_dict['prev'],'next':res_dict['next'],'thisRes':res_dict['res_atoms']}
    smallRTF = rtf_dict['thisResRTF']
    bad_signs = re.compile("[-+*]") #to remove issues with +N -C, etc.
    lines_dict['bonds'] = get_prop(rtf_dict,atom_dict,lines_dict['bonds'],patchinfo,"bond",2)
    lines_dict['bonds'] = get_prop(rtf_dict,atom_dict,lines_dict['bonds'],patchinfo,"double",2)
    lines_dict['bonds'] = get_prop(rtf_dict,atom_dict,lines_dict['bonds'],patchinfo,"triple",2)
   # print(patchinfo)
   # lines_dict['angles'] = get_prop(rtf_dict,atom_dict,lines_dict['angles'],patchinfo,"angle",3)
   # lines_dict['dihe'] = get_prop(rtf_dict,atom_dict,lines_dict['dihe'],patchinfo,'dihe',4)
    lines_dict['impr'] = get_prop(rtf_dict,atom_dict,lines_dict['impr'],patchinfo,'impr',4)
    if not rtf_dict['fpatch'] and not rtf_dict['lpatch']:
        lines_dict['cmap'] = get_prop(rtf_dict,atom_dict,lines_dict['cmap'],patchinfo,'cmap',8)
    #any angles other than IMPR get generated automatically. Any DIHE generated will be matched against the IMPR list
    #and removed if they are in the IMPR list.
    #to do this, we do an n^2 procedure. We take our current list of bonds, which are 2-element sets
    #we intersect each set with others and check if we get a 3-element set.
    #This 3-element set then gets added to the list of all angles if it is not already present.
    #In theory, this should cover every single angle.
    #For dihe, we take it one step further - intersect angle with bonds. This should produce 4-element sets that are identical to the dihedrals.
    #dono/acce also need custom logic so we can handle blnk/blank
    donor_string = "dono" if "dono" in smallRTF.meta else "donor" if "donor" in smallRTF.meta else False
    #NOTE: These two MAY fail if an RTF is inconsistent with how it writes DONOR and ACCEPTOR lines....Check on better ways to do this!
    acce_string = "acce" if "acce" in smallRTF.meta else "acceptor" if "acceptor" in smallRTF.meta else False
    lines_dict['donors'] = get_prop(rtf_dict,atom_dict,lines_dict['donors'],patchinfo,donor_string,2)
    lines_dict['acce'] = get_prop(rtf_dict,atom_dict,lines_dict['acce'],patchinfo,acce_string,2)
    #no consistent definition within RTFFile class...
    #next come GROUP, which are always present but not super relevant
    for rtf in rtf_dict.values():
        bad_atom = False
        if type(rtf) is TopRes:
            if rtf.groups:
                for group in rtf.groups: #there have to be groups...but check anyway.
                    #if any of the atoms in the group needs to be deleted due to patching, skip the entire group
                    for delrec in patchinfo['delrecords']:
                        for atom in delrec:
                            if atom in group:
                                bad_atom = True
                    if bad_atom: #if you need to delete any of the atoms, you need to get out of here, since you're in a patch and those define a new group.
                        continue #break would tear us out of the whole loop, so we have to be careful
                if not bad_atom:
                    #get the atom number of the first atom in the group...
                    #a group looks like a list of atoms, each of which looks like:
                    #(original_atom_name, massPRM_atom_name, charge)
                    #so to get the first atom of the first group, you need to do group[0] to get the line, and group[0][0] to get the atom
                    for group in rtf.groups:
                        atomno = int(atom_dict['thisRes'][group[0][0]]) - 1
                        tot_charge = 0
                        for atom in group:
                            tot_charge += float(atom[2]) #is this the best way to handle it...? rounding errors?
                        charged = 1 if tot_charge == 0 else 2 #how to handle rounding errors...?
                        #now add both of these to groups, and a 0 for the ST2 water placeholder.
                        #TODO: Find out why patched ALA gets 10 2 0 and not 10 1 0. FInd out whether it matters as well
                        #Note: they PROBABLY don't matter in most cases. Still under investigation.
                        this_grp = (atomno,charged,0)
                        already_there = False
                        for oldgrp in lines_dict['groups']:
                            if oldgrp[0] == this_grp[0]: #can be same group, different charge...
                                already_there = True
                                break
                        if not already_there:
                            lines_dict['groups'].append(this_grp)
    return lines_dict

def fix_atom_params(lines_dict,rtf_dict,res_dict,atname,atomnum):
    """
    Auxiliary function for writePSF
    Takes a :class:`dict` object, one :class:`RTFFile` object, a :class:`res` object and a dictionary
    containing the list of all atoms in the molecule, which is updated with atom information in PSF format.
    """
    #if any of the following vars are missing you shouldn't be in this method.
    #first we check here whether the atom is in either first or last patches that are submitted with this
    found_atom = False
    #TODO: Come up with a more generic way of assigning these - we may have more patches.
    if rtf_dict['fpatch'] and atname in rtf_dict['fpatch'].chemDict:
        rtf_to_use = "fpatch"
    elif rtf_dict['lpatch'] and atname in rtf_dict['lpatch'].chemDict:
        rtf_to_use = "lpatch"
    elif rtf_dict['thisResRTF'] and atname in rtf_dict['thisResRTF'].chemDict:
        rtf_to_use = "thisResRTF"
    else:
        raise IOError("Atom type %s could not be found in topology file."%atname) #TODO: make this a better error message
    #so right now, we're going to take all of the RTFs, and change the way we do the branching, instead.
    #the way CHARMM does it, is by reading in all of the atoms in RESI records and setting that up
    #and then deleting atoms from the record when it sees a "DELETE ATOM" in a PRES record.
    #As such, the goal here should be to replicate that behavior - none of our atoms are going to be deleted
    #however, we can delete bonds that involve those atoms, while keeping others.
    #we keep those two, and then when we see a bond or angle or whatever that involves a deleted atom, ignore it.
    smallRTF = rtf_dict['thisResRTF']
    bigRTF = rtf_dict['bigRTF']
    segid = res_dict['segid']
    res = res_dict['res']
    #This is going to include patches, so it shouldn't matter much
    atomRTFName = rtf_dict[rtf_to_use].chemDict[atname]
    atomCharge = rtf_dict[rtf_to_use].chargeDict[atname]
    atomMassPRM = bigRTF.atom[atomRTFName]
    atomMassNumber = atomMassPRM['number'] if lines_dict['PSF_FORMAT'] == "old" else atomRTFName.upper()
    atomMass = atomMassPRM['mass']
    minus_sign = "-" if atomCharge< 0 else ""
    atomCharge = abs(atomCharge)
    #the following line looks pretty hacky because the new format does left-align for atom names (non-numeric), whereas the numbers were right-aligned.
    #thus I have to change whether we have a minus or not.
    format = "%10s %-6s%3s%-8s%1s%-4s%5s%-4s%9s  %1s%-1.6f       %7s%12s%10s%14s\n"
    if lines_dict['PSF_FORMAT'] == "new":
        format = format.replace("%9s","     %-6s")
    lines_dict['atoms'] += format%(atomnum,segid.upper().strip(),\
                                                                                    " ",res.resid," ",res.resName.upper()," ",atname.upper(),\
                                                                                    atomMassNumber,minus_sign,atomCharge,atomMass[:7],\
                                                                                    "0","0.00000","0.000000")
    return lines_dict
    #this will be the only case where fix_params has display/write logic. All other parts of
    #fix_params will be parsed up in writePSF

def get_prop(rtf_dict,atom_dict,prop_list,patchinfo,propname,proplength):
    """
    auxiliary function for fix_params
    takes a `class`:RTFFile object, and gets all the values for a certain property
    ("bond","double","triple","angle",etc.) by using parse_prop.
    """
    for rtfname,rtf in rtf_dict.items():
        if rtf and type(rtf) is TopRes:
            if propname in rtf.meta:
                for prop in rtf.meta[propname]:
                    prop_list = parse_prop(prop,atom_dict,prop_list,patchinfo,propname,proplength)
                    #that should get most of the bonds, but it won't get all of them.
    return prop_list

def parse_prop(line,atom_dict,prop_list,patchinfo,propname,proplength):
    """
    Auxiliary function for fix_params
    Takes a list of a certain property taken from a `class`:RTFFile object, the name of an atom,
    a dictionary correlating atom names to atom numbers, a list of all the current values of that property
    in this molecule (a list containing a bunch of `type`:set ), and populates this list.
    This also takes the number of fields that the property requires (e.g. bonds require 2, angles 3, dihe 4).
    propname is left in for checks that require special logic (dono, acce)
    rtfname is left in to make sure we don't get extra stuff (+ on the last residue, - on the first)
    """
    bad_signs = re.compile("[-+*]") #to remove issues with +N -C, etc. - TODO: Modify this function to actually USE them. They are needed.
    this_prop_orig = line.split()
    this_prop = [re.sub(bad_signs,"",x) for x in this_prop_orig]
    #now compare every pair within...
    isbond = False
    check_len = len(this_prop) -1 if len(this_prop) > 1 else 1
    j = 0
    while j < check_len:
        bad_atom = False
        thisprop_check = this_prop_orig[j:j+proplength] #this wopn't crash if you go out of bounds
        thisprop_list = this_prop[j:j+proplength]
        #first let's check for +/-
       # print(thisprop_check)
        for x in thisprop_check:
            if ("+" in x and "lpatch" in patchinfo['active_rtfs']) or ("-" in x and "fpatch" in patchinfo['active_rtfs']):
                bad_atom = True
        if not bad_atom: #don't overcheck things...only one of these two conditions is enough to exclude it
            for delrec in patchinfo['delrecords']: #check to see if we don't need to delete any atoms
            #if there's one bad atom in an angle, drop the whole angle, if there's one bad atom in a bond, drop the bond...
                for atom in delrec:
                    if atom in thisprop_list: #seems we do
                        bad_atom = True #kill it
        if not bad_atom:
            if propname in ["dono","acce","donor","acceptor"]:
                don1 = atom_dict['thisRes'][thisprop_list[0]] if thisprop_list[0] != "blnk" else 0
                don2 = 0 if len(thisprop_list) < 2 else atom_dict['thisRes'][thisprop_list[1]] if thisprop_list[1] != "blnk" else 0
                newprop = frozenset([don1,don2])
            else:
                setlist = ()
               # print(thisprop_list)
               # print(atom_dict['thisRes'])
                for i in range(0,len(thisprop_check)):
                    x = thisprop_check[i]
                    fetch_from = "prev" if "-" in x else "next" if "+" in x else False
                    check_x = thisprop_list[i] #get it instead of running the re again
                   # print(patchinfo)
                    if fetch_from:
                        setlist = setlist + (atom_dict[fetch_from][check_x],)
                    else:
                        setlist = setlist + (atom_dict['thisRes'][check_x],)
               # setlist = [atom_dict[x] for x in thisprop_list]
                if propname != "impr" and propname != "cmap":
                    if propname in ["bond","double","triple"]:
                        newprop = set(setlist)
                    else:
                        newprop = frozenset(setlist)
                else:
                    newprop = setlist
            if propname in ["bond","double","triple"]:
                if newprop not in prop_list:
                    prop_list.append(newprop)
            else:
                if newprop not in prop_list:
                    if propname in ["dono","acce","donor","acceptor"]:
                        prop_list[newprop] = tuple(newprop)
                    else:
                        prop_list[newprop] = setlist
        j += proplength
    return prop_list
