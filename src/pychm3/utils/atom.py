def reorder(atoms):
  number = 1
  residue_id = 0
  residue_index = 0
  current_residue = None
  current_segment = None

  for atom in atoms:
    atom.number = number

    if atom.segment_id != current_segment:
      residue_id = 0
      current_segment = atom.segment_id

    if atom.residue_index != current_residue:
      current_residue = atom.residue_index
      residue_index += 1
      residue_id += 1

    atom.residue_id = residue_id
    atom.residue_index = residue_index

    number += 1

  return atoms
