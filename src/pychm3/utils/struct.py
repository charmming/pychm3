def reindex(struct, attr, index):
  if not hasattr(struct, '__iter__'):
    return False

  if not hasattr(struct[0], attr):
    return False

  for item in struct:
    if not hasattr(item, attr):
      continue

    setattr(item, attr, index)

    index+=1

  return True
