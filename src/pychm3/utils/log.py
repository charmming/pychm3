import logging

def log(message, **kwargs):
  level = kwargs.get('level', 'info')

  logger = logging.getLogger('pychm')

  if not hasattr(logger, level):
    level = 'info'

  log_method = getattr(logger, level)

  log_method(message)
