def loose_float(k):
    """Casts a variable to a float, but doesn't return exceptions on
    uninitialized (`None`) values.
    """
    try:
        return float(k)
    except TypeError:
        if k is None:
            return None
        else:
            raise

def loose_int(k):
    """Casts a variable to an int, but doesn't barf exceptions on
    uninitialized (`None`) values.
    """
    try:
        return int(k)
    except TypeError:
        if k is None:
            return None
        else:
            raise
