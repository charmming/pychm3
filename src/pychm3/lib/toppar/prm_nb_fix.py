from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float

import warnings

class PRMNBFix(BasePRM):
    """Object representing binary nonbonding VDW parameter constant overrides.
    These parameters explicitly override pairwise parameters procedurally
    generated bu Nonbond parameters.

    The first two arguments are strings representing atomtypes. During
    initialization these strings are normalized, such that 'atom0' is always
    the "lesser" string, allowing NBFixPRM objects to be hashed correctly.
    Changing the 'atom' attributes after initialization is *not* recommended
    because uniqueness is not guaranteed. For example, NBFix(AB) == NBFix(BA)
    if set at initialization, but not equal if set afterwards. The arguments
    'k' and 'eq' are the LJ 6-12 well depth and equilibrium distances,
    respectively. The last two arguments are optional, and are used for 1-4
    nonbonded interactions.
    """
    __slots__ = ['atom0', 'atom1', 'k', 'eq', 'k14', 'eq14', 'comment']

    def __init__(self, atom0=None, atom1=None, k=None, eq=None, k14=None, eq14=None, comment=None):
        self.atom0, self.atom1 = sorted((atom0, atom1))
        self.k = loose_float(k)
        self.eq = loose_float(eq)
        self.k14 = loose_float(k14)
        self.eq14 = loose_float(eq14)
        self.comment = comment

    @property
    def _sortkey(self):
        return (self.atom0, self.atom1)

    def _validate(self):
        for attrname in self.__slots__[:4]:
            if getattr(self, attrname) is None:
                warnings.warn("%r has uninitialized attr: %s" % (self, attrname))
        if self.ig14 is None and (self.k14 is not None or self.eq14 is not None):
            warnings.warn("%r has uninitialized attr: ig14" % self)
        if self.k14 is None and (self.ig14 is not None or self.eq14 is not None):
            warnings.warn("%r has uninitialized attr: k14" % self)
        if self.eq14 is None and (self.ig14 is not None or self.k14 is not None):
            warnings.warn("%r has uninitialized attr: eq14" % self)
