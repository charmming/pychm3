from pychm3.lib.toppar import PRMDihedral

class PRMImproper(PRMDihedral):
    """Object representing improper angle parameter constants.

    The first four arguments are strings representing atomtypes. During
    initialization these strings are normalized, such that 'atom0' is always
    the "lesser" string, allowing DihedralPRM objects to be hashed correctly.
    Changing the 'atom' attributes after initialization is *not* recommended
    because uniqueness is not guaranteed. For example, Improper(ABCD) ==
    Improper(DCBA) if set at initialization, but not equal if set afterwards.
    Furthermore, the multiplicity of the Improper, 'mult' is also used when
    determining parameter uniqueness. Changing the spring constant, 'k' and the
    equilibrium bond length 'eq' is fine at any time.
    """
    __slots__ = ['atom0', 'atom1', 'atom2', 'atom3' 'k', 'mult', 'eq', 'comment']
