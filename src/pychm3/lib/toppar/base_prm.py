from pychm3.lib.toppar import PRM

class BasePRM(object, metaclass=PRM):
    """Foundation class for PRM objects. Can not be instantiated because it
    lacks `abstract` definitions from class:`PRM`.

    All methods defined here depend upon :attr:`_sortkey`, a property defining
    how PRM objects are hashed. This hash is in turn used for sorting and
    determining uniqueness.
    """

    # Magic Methods #################################################
    def __hash__(self):
        return hash(self._sortkey)

    def __repr__(self):
        return '%s%r' % (self.__class__.__name__, self._sortkey)

    # Comparison Methods #########################################
    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False
        return self._sortkey == other._sortkey

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        return self._sortkey < other._sortkey

    def __le__(self, other):
        return self._sortkey <= other._sortkey

    def __gt__(self, other):
        return self._sortkey > other._sortkey

    def __ge__(self, other):
        return self._sortkey >= other._sortkey
