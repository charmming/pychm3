from pychm3.lib.toppar import BasePRM

class PRMCmap(BasePRM):
    """Object representing CHARMM CMAP parameters. It is currently a black box.
    """
    __slots__ = ['text']

    def __init__(self, arg=None):
        self.text = arg

    @property
    def _sortkey(self):
        return ('CMAP_BLOCK',)

    def _validate(self):
        pass
