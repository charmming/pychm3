from abc import ABCMeta, abstractmethod, abstractproperty

class PRM(ABCMeta):
    """A dummy :class:`abc.ABCMeta`-class, serves as a registrar and API
    definition.
    """
    # API Definition ################################################
    @abstractproperty
    def _sortkey(self):
        raise NotImplementedError

    @abstractmethod
    def _validate(self):
        raise NotImplementedError
