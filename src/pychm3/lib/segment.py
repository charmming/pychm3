from pychm3.lib.basestruct import BaseStruct
from pychm3.lib.residue import Residue
from pychm3.lib.protein import Protein

from functools import reduce

class Segment(BaseStruct):
    """
    Properties
        `addr`
        `chainid`
        `segid`
        `segType`
    """
    def __init__(self, iterable=None, **kwargs):
        super().__init__(iterable, **kwargs)

    ##############
    # Properties #
    ##############

    @property
    def addr(self):
        """
        The `addr` property provides a human readable unique string
        representation for each `Seg` instance.
        """
        return "{}.{}".format(self.chain_id, self.type)

    @property
    def residues(self):
      def reducer(memo, i):
        if len(memo[-1]) == 0:
          memo[-1].append(i)
        else:
          if memo[-1][-1].residue_index != i.residue_index:
            memo.append([])

          memo[-1].append(i)

        return memo

      if self.type == 'pro':
        ResidueClass = Protein
      else:
        ResidueClass = Residue

      residues = [ ResidueClass(iterable=iterable, code=self.code, autofix=False) for iterable in reduce(reducer, self.atoms, [[]])]

      return residues

    @property
    def type(self):
      return self.atoms[0].segment_type

    @type.setter
    def type(self, type):
        for atom in self:
            atom.segment_type = type

    @property
    def id(self):
      return self.atoms[0].segment_id

    @property
    def chain_id(self):
      return self.atoms[0].chain_id

    @property
    def atoms(self):
      return [atom for atom in self]
