class Bond(object):
    """
    DOCME
    """
    def __init__(self, atom1, atom2):
        self.__data = sorted([atom1, atom2])

    @property
    def i(self):
      return self.__data[0]

    @property
    def j(self):
      return self.__data[1]

    @property
    def key(self):
      return (self.i.addr, self.j.addr)

    @property
    def length(self):
      return self.i.calc_length(self.j)

    def _sort(self):
        return 1e12 * self.i._sort() + self.j._sort()

    def __repr__(self):
        return '%s%r' % (self.__class__.__name__, self.key)

    def __hash__(self):
        return hash(self.key)

    def __eq__(self, other):
        return self._data == other._data

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        return self._sort() < other._sort()

    def __le__(self, other):
        return self._sort() <= other._sort()

    def __gt__(self, other):
        return self._sort() > other._sort()

    def __ge__(self, other):
        return self._sort() >= other._sort()
