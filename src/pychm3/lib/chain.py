from pychm3.lib.basestruct import BaseStruct
from pychm3.lib.segment import Segment

class Chain(BaseStruct):
    """
    DOCME
    """
    def __init__(self, iterable=None, **kwargs):
        super(Chain, self).__init__(iterable, **kwargs)

##############
# Properties #
##############

    @property
    def addr(self):
        """
        The `addr` property provides a human readable unique string
        representation for each `Chain` instance.
        """
        return "{}".format(self.id)

    @property
    def atoms(self):
      return [atom for atom in self]

    @property
    def id(self):
      return self.atoms[0].chain_id

    @id.setter
    def id(self, id):
        for atom in self.atoms:
            atom.chain_id = id

    @property
    def segments(self):
        segment_types = list(set([ atom.segment_type for atom in self ]))
        iterators = [ [ atom for atom in self if atom.segment_type == segment_type ] for segment_type in segment_types ]
        segments = [ Segment(iterable=iterator, code=self.code, autoFix=False) for iterator in iterators ]

        return segments

    @property
    def residues(self):
      return [residue for segment in self.segments for residue in segment.residues]
