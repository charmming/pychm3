#!/usr/bin/python

"""
This program takes a coordinate file and a corresponding
set of at least one RTF, and creates a PSF by combining
information from both sources.
This program has been optimized many times, however,
by using the ctypes library and rebuilding the 
PDB and RTFFile classes to use a more efficient parsing
method, it would be possible to further optimize this, potentially
running at under 50% of the time it takes for
CHARMM to do the same thing.
~VS 9/1/14
"""
import sys
import os
from pychm3.io.pdb import PDB
from pychm3.io.pdb import get_mol_from_crd
from pychm3.io.rtf import RTFFile
import argparse
import traceback
import time

parser = argparse.ArgumentParser(description='Takes a coordinate file with residue information, as well as residue topology file(s) (RTF)\
                                and generates a PSF with this information.')
parser.add_argument('-f', '--filetype', required=True, dest='filetype', nargs=1, choices=['PDB','CRD'], action="store", help='Type of coordinate file used for input. Currently supported formats are PDB and CRD.')
parser.add_argument('-i', '--input', required=True, dest='inpfile', nargs=1, action="store", help="The path to the coordinate file used for input.")
parser.add_argument('-r', '--rtffile', required=True, dest='rtffiles', nargs="*", action="append", help="List of RTF files used for writing PSFs for this system.")
parser.add_argument('-p', '--psftype', dest='psftype', action='store', help='Type of PSF that will be written. Currently this only supports old-style (numerical atom names) and new-style (character-based atom names) formats. Short format is not supported at this time.')
parser.add_argument('-o', '--outputfile', dest='outfile', action='store', help='Directory for output of the PSFs of your system. If the -s flag is set, this becomes the "base" for your file name. For example,\
                    if your structure has three segments, "a-good", "a-bad" and "a-pro", and you set this flag to "psfout", then the program will write "psfout-a-good.psf", "psfout-a-bad.psf" and "psfout-a-pro.psf".')
parser.add_argument('-s', '--segmentwrite', dest='write_seg', action='store_true', help="Split the structure into segments as defined by pychm, and write each of these segments to its own PSF.")
args = vars(parser.parse_args(sys.argv[1:]))
#now args is a dictionary! Let's access all the stuff in here.

inpobj = False #holds the Mol object that we're using to generate the PSF
rtflist = [] #holds all the RTF files that the user provides
out_file = args['outfile'] if 'outfile' in args else False
inpfile = ""
try:
    inpfile = args['inpfile'][0]
    os.stat(inpfile)
except:
    print("Could not find input coordinate file at %s. Please provide an existing file as input."%args['inpfile'])
try:
    if args['filetype'][0] == "PDB":
        inpobj = next(iter(PDB(inpfile))) #models don't matter - they don't change the reslist or its order
    elif args['filetype'][0] == "CRD":
        inpobj = get_mol_from_crd(inpfile)
except:
    print("Invalid coordinate file. Please verify that your %s file is valid."%args['filetype'])
    sys.exit(1)
try:
    curr_rtf = False
    for arf in args['rtffiles']: #for some reason it double-packs the list...
        for rtf in arf:
            curr_rtf = rtf
            testrtf = RTFFile(rtf)
            rtflist.append(testrtf) #make objects of all the ones we need
except:
    print("%s is invalid. Please verify that it is in CHARMM36 format."%("Your RTFFile at "+curr_rtf if curr_rtf else "One of your RTF files"))
    traceback.print_exc()
    sys.exit(1)
#TODO: put in logic for handling psftype
#total = 0.0
#for i in range(0,100):
#    t0 = time.time()
inpobj.parse() #make them CHARMMing compliant - this wipes out AMET/BMET and other "fun" errors like that
write_seg = args['write_seg']
psftype = args['psftype'] #the writePSF method handles weird inputs, so we don't deal with it here
if out_file:
    inpobj.writePSF(rtflist,out_file=out_file,write_seg=write_seg,psf_format=psftype)
else:
    inpobj.writePSF(rtflist,write_seg=write_seg,psf_format=psftype)
#t1 = time.time()
#total += t1-t0
#print(str(total / 100.0)+"s")
